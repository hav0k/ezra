require "open-uri"
require 'ext/hash'
require 'ext/avito_api'
require 'ext/ebay_parse'
require 'ext/newauction_parse'
require 'ext/alib_parse'
require 'ext/oldbook_parse'
require 'ext/bidspirit_parse'
require 'ext/artelbook_parse'
require 'ext/telegram_log'


require 'google/apis/sheets_v4'
require 'googleauth'
require 'googleauth/stores/file_token_store'

# requires socksify gem
require "socksify"
require 'socksify/http'

# Mechanize: call @agent.set_socks(addr, port) before using
# any of it's methods; it might be working in other cases,
# but I just didn't tried :)
class Mechanize::HTTP::Agent
public
  def set_socks addr, port
    set_http unless @http
    class << @http
      attr_accessor :socks_addr, :socks_port

      def http_class
        Net::HTTP.SOCKSProxy(socks_addr, socks_port)
      end
    end
    @http.socks_addr = addr
    @http.socks_port = port
  end
end

require 'net/telnet'

