# config valid only for Capistrano 3.1
#lock '3.6.1'
lock '3.7.2'

set :application, 'ezra'
set :repo_url, 'git@bitbucket.org:hav0k/ezra.git'

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app
set :deploy_to, '/app/ezra'

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []

# set :linked_files, %w{config/database.yml}
append :linked_dirs, 'tmp/pids'
append :linked_dirs, 'public/oldbooks'


# Default value for linked_dirs is []
# set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 5

namespace :monit do
  desc "Task description"
  task :stop do
    on roles(:app) do
      sudo 'monit unmonitor rake-ezra'
      sudo 'monit stop thin-ezra'
      sudo '/app/ezra/current/rake_ezra.sh stop'
    end
  end

  desc "Task description"
  task :start do
    on roles(:app) do
      sudo 'monit monitor rake-ezra'
      sudo 'monit monitor thin-ezra'
      sudo 'monit start rake-ezra'
      sudo 'monit start thin-ezra'
    end
  end
end

namespace :deploy do
  before :deploy, "monit:stop"
  after :deploy, "monit:start"
end
