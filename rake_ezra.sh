#!/bin/bash
export PATH=$PATH:/usr/local/lib/ruby/2.3.0/rubygems

case $1 in
    start)
      cd /app/ezra/current;
      unbuffer /usr/local/bin/bundle exec rake loop_m RAILS_ENV=production >> /app/ezra/current/log/rake_ezra.log &
      ;;
    stop)
      kill `cat /app/ezra/current/tmp/pids/loop_m.pid`
      while ps -p `cat /app/ezra/current/tmp/pids/loop_m.pid` > /dev/null; do sleep 1; done
      rm -rf /app/ezra/current/tmp/pids/loop_m.pid
      echo 'End pid!'
      ;;
    *)
      echo "usage: ezra_worker {start|stop}" ;;
esac
exit 0


