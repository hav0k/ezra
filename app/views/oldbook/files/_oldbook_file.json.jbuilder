json.extract! oldbook_file, :id, :path, :task_id, :created_at, :updated_at
json.url oldbook_file_url(oldbook_file, format: :json)