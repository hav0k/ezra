class Iml::Point < ActiveRecord::Base
  belongs_to :city


  scope :my_order, -> { order(city_id: :asc).order(metro: :asc).order(address_formatted: :asc) }
end
