class Avito::Task < ActiveRecord::Base

  belongs_to :user, :class_name => User
  belongs_to :gdrive, :class_name => Gdrive
  has_many :avito_tasklogs, :class_name => Avito::Tasklog
  
  belongs_to :notifi_template, :class_name => Notifi::Template, :foreign_key => :notifi_template_id
  
  
  validates :p, presence: true
  validates :name, presence: true

  #validates :e, presence: true
  
end
