class Oldbook::Task < ActiveRecord::Base

  has_many :products, :class_name => Oldbook::Product
  has_many :files,    :class_name => Oldbook::File

end
