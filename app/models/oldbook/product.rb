class Oldbook::Product < ActiveRecord::Base
	belongs_to :task, :class_name => Oldbook::Task
	belongs_to :category, :class_name => Oldbook::Category

  def self.set h, category_id, task_id
    current_product = Oldbook::Product.find_by(:id => h[:id])
    uuid = UUID.new


    if current_product.nil?


      current_product = Oldbook::Product.new(
        id:           h[:id], 
        url:          h[:url],         
        path:         h[:path],
        sku:          h[:sku],
        task_id:      task_id, 
        category_id:  category_id, 
        uid:          uuid.generate,
        parse:        true,
        imgs:         h[:imgs],
        year:         h[:year], 
        sale:         h[:sale],     
        j:            h[:j],
        status:       "new"
      )
      current_product.save

      puts "Добавленна новая запись.".colorize(:green)
      
      current_product
    else

      if current_product.imgs != h[:imgs]
        current_product.imgs   = h[:imgs]
        current_product.status = "upd"
        current_product.upd_fields << "imgs"
      end

      ij = 0
      h[:j].map{|hj|
        if current_product.j[hj[0]] != hj[1]
          current_product.upd_fields << hj[0]
          ij = 1
        end
      }

      if ij == 1
        current_product.status = "upd"
        current_product.j      = h[:j]
      end

      if current_product.upd_fields.blank?
        puts "Запись #{h[:id]} актуальна.".colorize(:green)
        current_product.status = "old"
      else
        puts "Запись #{h[:id]} обновленна - #{current_product.upd_fields.join(', ')}.".colorize(:green)
      end

      current_product.save
    end
  end

end
