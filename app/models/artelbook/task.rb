class Artelbook::Task < ActiveRecord::Base

  has_many :products, :class_name => Artelbook::Product
  has_many :files,    :class_name => Artelbook::File

end
