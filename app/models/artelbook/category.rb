class Artelbook::Category < ActiveRecord::Base
	has_many :products, :class_name => Artelbook::Product

  def self.set hash
    category = Artelbook::Category.find_by(:path => hash[:path])
    uuid     = UUID.new

    if category.nil?
      category = Artelbook::Category.new(
        :path => hash[:path],
        :name => hash[:name],
        :uid  => uuid.generate
        )
      category.save
      p category
    end

    category
  end

end
