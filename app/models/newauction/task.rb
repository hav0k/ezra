class Newauction::Task < ActiveRecord::Base
  belongs_to :user, :class_name => User
  belongs_to :gdrive, :class_name => Gdrive
  has_many :newauction_tasklogs, :class_name => Newauction::Tasklog
  
  belongs_to :notifi_template, :class_name => Notifi::Template, :foreign_key => :notifi_template_id

  validates :name, presence: true

end
