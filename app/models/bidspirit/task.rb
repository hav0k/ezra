class Bidspirit::Task < ActiveRecord::Base
  belongs_to :gdrive, :class_name => Gdrive
  has_many :bidspirit_tasklogs, :class_name => Bidspirit::Tasklog
  
  belongs_to :notifi_template, :class_name => Notifi::Template, :foreign_key => :notifi_template_id

  validates :name, presence: true
end
