class Setting < ActiveRecord::Base

  def self.get(s)
    setting = Setting.find_by(:name => s)

    if setting.nil?
      v=false
    else
      case setting.t
      when "integer"
        v = setting.v.to_i
      when "string"
        v = setting.v
      when "text"
        v = setting.v
      when "json"
        v = setting.vh
      else
        v = nil
      end

      v
    end
  end


  def self.set(s,v)
    setting = Setting.find_by(:name => s)

    if setting.nil?
      false
    else
      case setting.t
      when "integer"
        setting.v = v
      when "string"
        setting.v = v
      when "text"
        setting.v = v
      when "json"
        setting.vh = v
      else

      end

      setting.save
    end


  end

end
