class Gdrive < ActiveRecord::Base
  has_many :avito_tasks,     :class_name => Avito::Task
  has_many :newauction_task, :class_name => Newauction::Task
  has_many :ebay_task,       :class_name => Ebay::Task
  has_many :alib_task,       :class_name => Alib::Task
  has_many :bidspirit_task,  :class_name => Bidspirit::Task
end
