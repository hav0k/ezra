class Notifi::Template < ActiveRecord::Base
  has_many :notifi_contents, :class => Notifi::Content, :foreign_key => :notifi_template_id

  has_many :avito_tasks,        :class => Avito::Task
  has_many :ebay_tasks,         :class => Ebay::Task
  has_many :newauction_tasks,   :class => Ebay::Task  
  has_many :alib_tasks,         :class => Alib::Task 
  has_many :bidspirit_tasks,    :class => Bidspirit::Task  

  belongs_to :user, :class => User

  validates :name, presence: true


end
