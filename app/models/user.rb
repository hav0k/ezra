class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  #devise :invitable, :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable

  #:registerable

  devise :database_authenticatable, :recoverable, :confirmable, :validatable, :invitable, :invite_for => 2.weeks
  
  has_many :avito_tasks,    :class_name => Avito::Task
  has_many :avito_postings, :class_name => Avito::Posting
  has_many :avito_accounts, :class_name => Avito::Account
  has_many :avito_finds,    :class_name => Avito::Find
  
  has_many :notifi_templates, :class_name => Notifi::Template
  
  has_many :newauction_task, :class_name => Newauction::Task
  has_many :ebay_task,       :class_name => Ebay::Task
  has_many :alib_task,       :class_name => Alib::Task

  has_many :images
  
  belongs_to :role

end
