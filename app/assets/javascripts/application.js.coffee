#= require jquery
#= require jquery_ujs
#= require_tree .

#= require foundation

#= require dropzone
#= require chosen.jquery
#= require chosenajax.jquery
#= require sigma
#= require col
#= require main

() ->
  $(document).foundation()
