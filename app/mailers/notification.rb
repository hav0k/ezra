class Notification < ActionMailer::Base
  default from: "parser@liderknig.ru"

  def mail_medium(mail, ad)
    m=mail(to: mail, subject: "#{ad.count} – Новых обьявлений") do |format|
      @ad = ad
      format.html
    end
    m.deliver

    p smtp_settings
  end

  def mail_notifi(mail, ad, st)
    date = Time.now.strftime('%d.%m.%y')
    m=mail(to: mail, subject: "Новые объявления с Avito, Ebay, NewAuction, Alib – #{date}") do |format|
      @ad = ad
      @st = st
      format.html
    end
    m.deliver
    p smtp_settings
  end

  def mail_files(mail, ad, files, folder)

    date = Time.now.strftime('%d.%m.%y')
    m=mail(to: mail, subject: "Выгрузка #{folder}– #{date}") do |format|
      @ad    = ad
      @files = files
      @folder= folder
      format.html
    end

    m.deliver
    p smtp_settings
  end

  def mail_alib_shop(mail, new_b, old_b, task)
    date = Time.now.strftime('%d.%m.%y')
    m=mail(to: mail, subject: "Мониторинг Alib – #{task.q}/#{date}") do |format|
      @new_b = new_b
      @old_b = old_b
      @task  = task
      format.html
    end

    m.deliver
    p smtp_settings
  end


  def mail_iml_new(mail, arr=nil)
    if !arr.blank?
      date = Time.now.strftime('%d.%m.%y')
      m=mail(to: mail, subject: "Обновления Iml #{date}") do |format|
        @arr = arr
        format.html
      end

      m.deliver
      p smtp_settings
    end
  end



end
