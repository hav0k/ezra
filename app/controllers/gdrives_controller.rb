class GdrivesController < ApplicationController
  before_action :set_gdrive, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @gdrives = Gdrive.all
    respond_with(@gdrives)
  end

  def show
    @service = Google::Apis::SheetsV4::SheetsService.new
    @service.client_options.application_name = "Erza"
    @service.authorization = authorize

    @ss = check_sheet(@gdrive.spreadsheet)

    if @ss
      response = @service.get_spreadsheet_values(@gdrive.spreadsheet, "A1:A")
      @rows    = response.values.map{|i| i[0] }
      respond_with(@gdrive)
    else
      redirect_to :action => 'edit'
    end

  end

  def new
    @gdrive = Gdrive.new
    respond_with(@gdrive)
  end

  def edit
  end

  def create
    @service = Google::Apis::SheetsV4::SheetsService.new
    @service.client_options.application_name = "Erza"
    @service.authorization = authorize
    @gdrive = Gdrive.new(gdrive_params)
    
    if check_sheet(@gdrive.spreadsheet)
      @gdrive.save
      respond_with(@gdrive)
    else
      redirect_to :action => 'edit'
    end
  end


  def update
    @service = Google::Apis::SheetsV4::SheetsService.new
    @service.client_options.application_name = "Erza"
    @service.authorization = authorize
    
    if check_sheet(gdrive_params[:spreadsheet])
      @gdrive.update(gdrive_params)
      respond_with(@gdrive)
    else
      redirect_to :action => 'edit'
    end
  end

  def destroy
    @gdrive.destroy
    respond_with(@gdrive)
  end

  private
    def set_gdrive
      @gdrive = Gdrive.find(params[:id])
    end

    def gdrive_params
      params.require(:gdrive).permit(:name, :spreadsheet, :enable)
    end

    def authorize
      client_id = Google::Auth::ClientId.new(
        Setting.get('client_id'    ), 
        Setting.get('client_secret')
      )
      scope = [
        Google::Apis::SheetsV4::AUTH_SPREADSHEETS,
        Google::Apis::SheetsV4::AUTH_DRIVE,
      ]

      token_store = Google::Auth::Stores::FileTokenStore.new(file: 'config/sheets.googleapis.yaml')
      authorizer = Google::Auth::UserAuthorizer.new(client_id, scope, token_store)
      user_id = 'main'
      credentials = authorizer.get_credentials(user_id)
      if credentials.nil?
        render :json => "Err token"
      end
      credentials
    end


    def check_sheet(s)
      begin
        @service.get_spreadsheet(s)
      rescue Exception => e
        false
      end
    end


end
