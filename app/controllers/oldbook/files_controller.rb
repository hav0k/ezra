class Oldbook::FilesController < ApplicationController
  before_action :set_oldbook_file, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!


  respond_to :html

  def index
    @oldbook_files = Oldbook::File.all
    respond_with(@oldbook_files)
  end

  def show
    respond_with(@oldbook_file)
  end

  def new
    @oldbook_file = Oldbook::File.new
    respond_with(@oldbook_file)
  end

  def edit
  end

  def create
    @oldbook_file = Oldbook::File.new(file_params)
    @oldbook_file.save
    respond_with(@oldbook_file)
  end

  def update
    @oldbook_file.update(file_params)
    respond_with(@oldbook_file)
  end

  def destroy
    path = @oldbook_file.path
    @oldbook_file.destroy
    File.unlink("#{Rails.root}/public/oldbooks/xlsx/#{path}")
    redirect_to :back
  end

  private
    def set_oldbook_file
      @oldbook_file = Oldbook::File.find(params[:id])
    end

    def oldbook_file_params
      params.require(:oldbook_file).permit(:path, :task_id)
    end
end
