class Oldbook::TasksController < ApplicationController
  before_action :set_oldbook_task, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!


  respond_to :html

  def index
    @oldbook_tasks = Oldbook::Task.all
    respond_with(@oldbook_tasks)
  end

  def show
    respond_with(@oldbook_task)
  end

  def new
    @oldbook_task = Oldbook::Task.new
    respond_with(@oldbook_task)
  end

  def edit
  end

  def create
    @oldbook_task = Oldbook::Task.new(task_params)
    @oldbook_task.save
    respond_with(@oldbook_task)
  end

  def update
    @oldbook_task.update(task_params)
    respond_with(@oldbook_task)
  end

  def destroy
    @oldbook_task.destroy
    respond_with(@oldbook_task)
  end

  private
    def set_oldbook_task
      @oldbook_task = Oldbook::Task.find(params[:id])
    end

    def task_params
      params.require(:oldbook_task).permit(:name, :path, :enable)
    end
end
