class Oldbook::CategoriesController < ApplicationController
  before_action :set_oldbook_category, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!


  respond_to :html

  def index
    @oldbook_categories = Oldbook::Category.all
    respond_with(@oldbook_categories)
  end

  def show
    respond_with(@oldbook_category)
  end

  def new
    @oldbook_category = Oldbook::Category.new
    respond_with(@oldbook_category)
  end

  def edit
  end

  def create
    @oldbook_category = Oldbook::Category.new(category_params)
    @oldbook_category.save
    respond_with(@oldbook_category)
  end

  def update
    @oldbook_category.update(category_params)
    respond_with(@oldbook_category)
  end

  def destroy
    @oldbook_category.destroy
    respond_with(@oldbook_category)
  end

  private
    def set_oldbook_category
      @oldbook_category = Oldbook::Category.find(params[:id])
    end

    def category_params
      params.require(:oldbook_category).permit(:name, :uid)
    end
end
