class Oldbook::ProductsController < ApplicationController
  before_action :set_oldbook_product, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!


  respond_to :html

  def index
    @oldbook_products = Oldbook::Product.all
    respond_with(@oldbook_products)
  end

  def show
    respond_with(@oldbook_product)
  end

  def new
    @oldbook_product = Oldbook::Product.new
    respond_with(@oldbook_product)
  end

  def edit
  end

  def create
    @oldbook_product = Oldbook::Product.new(product_params)
    @oldbook_product.save
    respond_with(@oldbook_product)
  end

  def update
    @oldbook_product.update(product_params)
    respond_with(@oldbook_product)
  end

  def destroy
    @oldbook_product.destroy
    respond_with(@oldbook_product)
  end

  private
    def set_oldbook_product
      @oldbook_product = Oldbook::Product.find(params[:id])
    end

    def oldbook_product_params
      params.require(:oldbook_product).permit(:path, :task_id, :category_id, :uid, :parse, :j)
    end
end
