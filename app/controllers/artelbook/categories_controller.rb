class Artelbook::CategoriesController < ApplicationController
  before_action :set_Artelbook_category, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!


  respond_to :html

  def index
    @artelbook_categories = Artelbook::Category.all
    respond_with(@artelbook_categories)
  end

  def show
    respond_with(@artelbook_category)
  end

  def new
    @artelbook_category = Artelbook::Category.new
    respond_with(@artelbook_category)
  end

  def edit
  end

  def create
    @artelbook_category = Artelbook::Category.new(category_params)
    @artelbook_category.save
    respond_with(@artelbook_category)
  end

  def update
    @artelbook_category.update(category_params)
    respond_with(@artelbook_category)
  end

  def destroy
    @artelbook_category.destroy
    respond_with(@artelbook_category)
  end

  private
    def set_Artelbook_category
      @artelbook_category = Artelbook::Category.find(params[:id])
    end

    def category_params
      params.require(:Artelbook_category).permit(:name, :uid)
    end
end
