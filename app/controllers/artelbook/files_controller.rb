class Artelbook::FilesController < ApplicationController
  before_action :set_Artelbook_file, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!


  respond_to :html

  def index
    @artelbook_files = Artelbook::File.all
    respond_with(@artelbook_files)
  end

  def show
    respond_with(@artelbook_file)
  end

  def new
    @artelbook_file = Artelbook::File.new
    respond_with(@artelbook_file)
  end

  def edit
  end

  def create
    @artelbook_file = Artelbook::File.new(file_params)
    @artelbook_file.save
    respond_with(@artelbook_file)
  end

  def update
    @artelbook_file.update(file_params)
    respond_with(@artelbook_file)
  end

  def destroy
    path = @artelbook_file.path
    @artelbook_file.destroy
    File.unlink("#{Rails.root}/public/Artelbooks/xlsx/#{path}")
    redirect_to :back
  end

  private
    def set_Artelbook_file
      @artelbook_file = Artelbook::File.find(params[:id])
    end

    def Artelbook_file_params
      params.require(:Artelbook_file).permit(:path, :task_id)
    end
end
