class Artelbook::ProductsController < ApplicationController
  before_action :set_Artelbook_product, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!


  respond_to :html

  def index
    @artelbook_products = Artelbook::Product.all
    respond_with(@artelbook_products)
  end

  def show
    respond_with(@artelbook_product)
  end

  def new
    @artelbook_product = Artelbook::Product.new
    respond_with(@artelbook_product)
  end

  def edit
  end

  def create
    @artelbook_product = Artelbook::Product.new(product_params)
    @artelbook_product.save
    respond_with(@artelbook_product)
  end

  def update
    @artelbook_product.update(product_params)
    respond_with(@artelbook_product)
  end

  def destroy
    @artelbook_product.destroy
    respond_with(@artelbook_product)
  end

  private
    def set_Artelbook_product
      @artelbook_product = Artelbook::Product.find(params[:id])
    end

    def Artelbook_product_params
      params.require(:Artelbook_product).permit(:path, :task_id, :category_id, :uid, :parse, :j)
    end
end
