class Artelbook::TasksController < ApplicationController
  before_action :set_Artelbook_task, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  respond_to :html

  def index
    @artelbook_tasks = Artelbook::Task.all

    if params[:task] == "run"
      run
      redirect_to action: 'index'
    else
      respond_with(@artelbook_tasks)
    end

  end

  def show
    respond_with(@artelbook_task)
  end

  def new
    @artelbook_task = Artelbook::Task.new
    respond_with(@artelbook_task)
  end

  def edit
  end

  def create
    @artelbook_task = Artelbook::Task.new(task_params)
    @artelbook_task.save
    respond_with(@artelbook_task)
  end

  def update
    @artelbook_task.update(task_params)
    respond_with(@artelbook_task)
  end

  def destroy
    @artelbook_task.destroy
    respond_with(@artelbook_task)
  end

  private
    def set_Artelbook_task
      @artelbook_task = Artelbook::Task.find(params[:id])
    end

    def task_params
      params.require(:artelbook_task).permit(:name, :path, :enable, :price_ratio, :sku_regex, :title_exclude)
    end

    def run
      unless File.exist?('tmp/artelbook_task')
        File.write('tmp/artelbook_task','')
      end
    end
end
