class Iml::CitiesController < ApplicationController
  before_action :set_iml_city, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!


  respond_to :html

  def index
    @iml_cities = Iml::City.all
    respond_with(@iml_cities)
  end

  def show
    respond_with(@iml_city)
  end

  def new
    @iml_city = Iml::City.new
    respond_with(@iml_city)
  end

  def edit
  end

  def create
    @iml_city = Iml::City.new(city_params)
    @iml_city.save
    respond_with(@iml_city)
  end

  def update
    @iml_city.update(city_params)
    respond_with(@iml_city)
  end

  def destroy
    @iml_city.destroy
    respond_with(@iml_city)
  end

  private
    def set_iml_city
      @iml_city = Iml::City.find(params[:id])
    end

    def iml_city_params
      params.require(:iml_city).permit(:name, :prefix, :zoom, :lat, :lon, :geo)
    end
end
