class Iml::PointsController < ApplicationController
  before_action :set_iml_point, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:inidex]

  respond_to :html

  def index
    case params[:gen]
    when "msk"
      @iml_points = Iml::City.find_by(:prefix => "/Moscow").points.my_order
      render layout: 'clear', template: '/iml/points/table'

    when 'spb'
      @iml_points = Iml::City.find_by(:prefix => "/Saint_Petersburg").points.my_order
      render layout: 'clear', template: '/iml/points/table'
    when 'oth'
      ids = Iml::City.where(:prefix => ["?Moscow","/Saint_Petersburg" ]).map{|i| i.id}
      @iml_points = Iml::Point.where.not(city_id: ids).my_order
      render layout: 'clear', template: '/iml/points/table2'
    else
      @iml_points = Iml::Point.my_order.all
      respond_with(@iml_points)
    end

  end

  def show
    respond_with(@iml_point)
  end

  def new
    @iml_point = Iml::Point.new
    respond_with(@iml_point)
  end

  def edit
  end

  def create
    @iml_point = Iml::Point.new(point_params)
    @iml_point.save
    respond_with(@iml_point)
  end

  def update
    @iml_point.update(point_params)
    respond_with(@iml_point)
  end

  def destroy
    @iml_point.destroy
    respond_with(@iml_point)
  end

  private
    def set_iml_point
      @iml_point = Iml::Point.find(params[:id])
    end

    def iml_point_params
      params.require(:iml_point).permit(:name, :link, :zoom, :address, :path, :time, :photo, :metro, :lat, :lon, :geo, :city_id)
    end
end
