class Notifi::TemplatesController < ApplicationController
  before_action :set_notifi_template, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  respond_to :html

  def index
    @notifi_templates = Notifi::Template.all
    respond_with(@notifi_templates)
  end

  def show
    if params[:content_id].nil?
      respond_with(@notifi_template)
    else
      @notifi_content = @notifi_template.notifi_contents.find_by(:id => params[:content_id])
      if @notifi_content.nil?
        respond_with(@notifi_template)    
      else
        render 'show_template'
      end
    end
  end

  def new
    @notifi_template = Notifi::Template.new
    respond_with(@notifi_template)
  end

  def edit
  end

  def create    
    @notifi_template = Notifi::Template.new(notifi_template_params)
    @notifi_template.s = params[:notifi_template][:s]
    @notifi_template.user_id = current_user.id
    @notifi_template.save
    respond_with(@notifi_template)
  end

  def update
    @notifi_template.s = params[:notifi_template][:s]
    @notifi_template.update(notifi_template_params)
    respond_with(@notifi_template)
  end

  def destroy
    @notifi_template.destroy
    respond_with(@notifi_template)
  end

  private
    def set_notifi_template
      @notifi_template = Notifi::Template.find_by(:id => params[:id])
    end

    def notifi_template_params
      params.require(:notifi_template).permit(:name, :s, :active, :log, :count, :interval, :next_at)
    end
end
