class Notifi::ContentsController < ApplicationController
  before_action :set_notifi_content, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  respond_to :html

  def index
    @notifi_contents = Notifi::Content.all
    respond_with(@notifi_contents)
  end

  def show
    respond_with(@notifi_content)
  end

  def new
    @notifi_content = Notifi::Content.new
    respond_with(@notifi_content)
  end

  def edit
  end

  def create
    @notifi_content = Notifi::Content.new(content_params)
    @notifi_content.save
    respond_with(@notifi_content)
  end

  def update
    @notifi_content.update(content_params)
    respond_with(@notifi_content)
  end

  def destroy
    @notifi_content.destroy
    redirect_to(:back)
  end

  private
    def set_notifi_content
      @notifi_content = Notifi::Content.find(params[:id])
    end

    def notifi_content_params
      params.require(:notifi_content).permit(:c, :notifi_template_id, :send)
    end
end
