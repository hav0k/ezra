class Avito::TasksController < ApplicationController
  before_action :set_task, only: [:show, :edit, :update, :destroy]
  respond_to    :html
  before_action :authenticate_user!
  before_action :notifi_teplates

  def index
    @tasks = Avito::Task.all
    respond_with(@tasks)
  end

  def show
    respond_with(@task)
  end

  def new
    @task = Avito::Task.new
    @notifi_templates = current_user.notifi_templates.where(:active => true)
    respond_with(@task)
  end

  def edit
  end


  def create
    swoop = Proc.new { |k, v| v.delete_if(&swoop) if v.kind_of?(Hash);  v.empty? }
    @task = Avito::Task.new(task_params.delete_if(&swoop))

    @task.user = current_user
    render :json => {:status=>@task.save, :result=>@task}
  end

  def update
    @task.update(task_params_strong)
    respond_with(@task)
  end

  def destroy
    @task.destroy
    respond_with(@task)
  end


  ###-_-_-_###
  private
    def set_task
      @task = Avito::Task.find_by(:id => params[:id])
    end

    def task_params
      params.require(:avito_task).permit(:name, :interval, :active, :notifi_template_id, :q,:gdrive_id).tap do |while_listed|
        while_listed[:p] = params[:task][:p]
        while_listed[:e] = params[:task][:e]
      end
    end

    def task_params_strong
      params.require(:avito_task).permit(:name, :interval, :active, :notifi_template_id, :q, :next_at, :gdrive_id)
    end

    def notifi_teplates
      @notifi_teplates = Notifi::Template.where(:active => true).all.collect {|p| [ p.name, p.id ] }
      @gdrive = Gdrive.where(:enable => true).all.collect {|p| [ p.name, p.id ] }
    end


end
