class Ebay::TasksController < ApplicationController
  before_action :set_ebay_task, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :notifi_teplates


  respond_to :html

  def index
    @ebay_tasks = Ebay::Task.all
    respond_with(@ebay_tasks)
  end

  def show
    respond_with(@ebay_task)
  end

  def new
    @ebay_task = Ebay::Task.new()
    respond_with(@ebay_task)
  end

  def edit
  end

  def create
    @ebay_task = Ebay::Task.new(task_params)
    @ebay_task.user_id = current_user.id
    @ebay_task.save
    respond_with(@ebay_task)
  end

  def update
    @ebay_task.update(task_params)
    respond_with(@ebay_task)
  end

  def destroy
    @ebay_task.destroy
    respond_with(@ebay_task)
  end

  private
    def set_ebay_task
      @ebay_task = Ebay::Task.find(params[:id])
    end

    def task_params
      params.require(:ebay_task).permit(:name, :q, :interval, :active, :next_at, :notifi_template_id, :gdrive_id)
    end

    def notifi_teplates
      @notifi_teplates = Notifi::Template.where(:active => true).all.collect {|p| [ p.name, p.id ] }
      @gdrive = Gdrive.where(:enable => true).all.collect {|p| [ p.name, p.id ] }
    end

end
