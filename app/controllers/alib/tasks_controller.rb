class Alib::TasksController < ApplicationController
  before_action :set_alib_task, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :notifi_teplates


  respond_to :html

  def index
    @alib_tasks = Alib::Task.all
    respond_with(@alib_tasks)
  end

  def show
    respond_with(@alib_task)
  end

  def new
    @alib_task = Alib::Task.new
    respond_with(@alib_task)
  end

  def edit
  end

  def create
    @alib_task = Alib::Task.new(task_params)
    @alib_task.save
    respond_with(@alib_task)
  end

  def update
    @alib_task.update(task_params)
    respond_with(@alib_task)
  end

  def destroy
    @alib_task.destroy
    respond_with(@alib_task)
  end

  private
    def set_alib_task
      @alib_task = Alib::Task.find(params[:id])
    end

    def task_params
      params.require(:alib_task).permit(:name, :q, :interval, :active, :next_at, :notifi_template_id, :shop, :gdrive_id)
    end

    def notifi_teplates
      @notifi_teplates = Notifi::Template.where(:active => true).all.collect {|p| [ p.name, p.id ] }
      @gdrive = Gdrive.where(:enable => true).all.collect {|p| [ p.name, p.id ] }
    end
end
