class Newauction::TasksController < ApplicationController
  before_action :set_newauction_task, only: [:show, :edit, :update, :destroy]
  before_action :notifi_teplates
  before_action :authenticate_user!

  respond_to :html

  def index
    @newauction_tasks = Newauction::Task.all
    respond_with(@newauction_tasks)
  end

  def show
    respond_with(@newauction_task)
  end

  def new
    @newauction_task = Newauction::Task.new
    respond_with(@newauction_task)
  end

  def edit
  end

  def create
    @newauction_task = Newauction::Task.new(task_params)
    @newauction_task.save
    respond_with(@newauction_task)
  end

  def update
    @newauction_task.update(task_params)
    respond_with(@newauction_task)
  end

  def destroy
    @newauction_task.destroy
    respond_with(@newauction_task)
  end

  private
    def set_newauction_task
      @newauction_task = Newauction::Task.find(params[:id])
    end

    def notifi_teplates
      @notifi_teplates = Notifi::Template.where(:active => true).all.collect {|p| [ p.name, p.id ] }
      @gdrive = Gdrive.where(:enable => true).all.collect {|p| [ p.name, p.id ] }
    end

    def task_params
      params.require(:newauction_task).permit(:name, :q, :interval, :active, :notifi_template_id, :next_at, :gdrive_id)
    end
end
