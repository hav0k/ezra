namespace :db do

  desc "Add setting in db"
  task :setting => :environment do
    if !Setting.get("stop_world")
      s=Setting.new(
        name: "stop_world",
        t: 'text',
        about: "Стоп слова",
        v: "Привет")
      s.save
    end

    if !Setting.get("email")
      s=Setting.new(
        name: "email",
        t: 'string',
        about: "Email",
        v: "Привет")
      s.save
    end

    if !Setting.get("sleep_get")
      s=Setting.new(
        name: "sleep_get",
        t: 'integer',
        about: "Пауза между запросамми - мс.",
        v: "999")
      s.save
    end

    if !Setting.get("sleep_get_paginate")
      s=Setting.new(
        name: "sleep_get_paginate",
        t: 'integer',
        about: "Пауза между запросамми постранично - мс.",
        v: "999")
      s.save
    end


    if !Setting.get("change_option_val")
      s=Setting.new(
        name: "change_option_val",
        t: "json",
        about: "Замена опции",
        v: nil,
        vh: [
          {"find"=>"СПБ", "value"=>"Санкт-Петербург"},
          {"find"=>"М.", "value"=>"Москва"}
        ]
      )
      s.save
    end

    if !Setting.get("client_id")
      s=Setting.new(
        name: "client_id",
        t: "string",
        about: "Google API Client ID",
        v: nil,
      )
      s.save
    end

    if !Setting.get("client_secret")
      s=Setting.new(
        name: "client_secret",
        t: "string",
        about: "Google API Client Secter",
        v: nil,
      )
      s.save
    end

    if !Setting.get("telegram_token")
      s=Setting.new(
        name: "telegram_token",
        t: "string",
        about: "Telegram API token secter",
        v: '396939620:AAHKCOdeC15FFwzGxY4rcJI58RID0LHreSQ',
      )
      s.save
    end

    if !Setting.get("telegram_chat_id")
      s=Setting.new(
        name: "telegram_chat_id",
        t: "string",
        about: "Telegram chat ID",
        v: '194171706',
      )
      s.save
    end




  end

  desc "Clear setting in db"
  task :setting_clear => :environment do
    Setting.delete_all
  end


  desc "Task description"
  task :clear => :environment do
    z=Avito::Task.last
    z.avito_tasklogs.delete_all
    z.notifi_template_id = 1
    z.next_at = "2015-10-10 07:29:38"
    z.active = true
    z.save

    z=Ebay::Task.last
    z.ebay_tasklogs.delete_all
    z.notifi_template_id = 1
    z.next_at = "2015-10-11 06:16:48"
    z.active = true
    z.save

    z=Newauction::Task.last
    z.newauction_tasklogs.delete_all
    z.notifi_template_id = 1
    z.next_at = "2015-10-11 06:16:48"
    z.active = true
    z.save

    z=Alib::Task.last
    z.alib_tasklogs.delete_all
    z.notifi_template_id = 1
    z.next_at = "2015-10-11 06:16:48"
    z.active = true
    z.save


    z = Bidspirit::Task.last
    z.bidspirit_tasklogs.delete_all
    z.next_at =  "2015-10-11 06:16:48"
    z.active = true
    z.save

    n = Notifi::Template.last
    n.notifi_contents.map{|i| i.delete}
    n.next_at =  "2015-10-11 06:16:48"
    n.active = true
    n.save


  end

desc "Task description"
task :tm => :environment do
  z=Avito::Task.last
  z.next_at = "2015-10-10 07:29:38"
  z.save

  z=Ebay::Task.last
  z.next_at = "2015-10-11 06:16:48"
  z.save

  z=Newauction::Task.last
  z.next_at = "2015-10-11 06:16:48"
  z.save

  z=Alib::Task.last
  z.next_at = "2015-10-11 06:16:48"
  z.save

  n = Notifi::Template.last
  n.next_at =  "2015-10-11 06:16:48"
  n.save
end

end
