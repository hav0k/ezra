namespace :oldbooks do

  desc "Парсинг сайта http://www.oldbooks.ru"
  task :parse => :environment do
    ######################################################################
    puts "Старт парсинга сайта http://www.oldbooks.ru".colorize(:green) ##
    @g = { :time_start => Time.now}                                     ##
    uuid = UUID.new

    ######################################################################

    tasks = Oldbook::Task.where(:enable => true)
    o     = OldbookParse.new()
    files_task = []
    files = {}

    tasks.each do |task|
      urls = [task.path]
      url  = urls.pop

      if 1==1 #г
        while !urls.blank? || !url.blank?

          case o.check(url)
          when "catalogs"
            urls += o.cats(url)

          when "products"
            urls += o.cats(url)

            products = o.products(url)
            if products[:products].blank?
              puts "Продуктов не найденно на #{url} / #{products[:path]} => #{products[:name]}".colorize(:yellow)
            else
              current_category = Oldbook::Category.set(products[:category])

              products[:products].each do |product_url|
                product = o.get(product_url)
                if product[:status]
                  current_product = Oldbook::Product.set(product[:result], current_category.id, task.id)
                else
                  puts "ошибка get - #{product_url}".colorize(:red)
                end
              end

            end

          else
            puts "неопределённый тип url #{url}".colorize(:red)
          end
          url = urls.pop
        end
      end

      #генерация файла
      file_name = "#{uuid.generate}.xlsx"
      files_task << {:path => file_name, :name => task.name}
      current_file = Oldbook::File.new(
        path:    file_name,
        task_id: task.id
      )

      if current_file.save
        workbook  = RubyXL::Workbook.new
        worksheet = workbook[0]
        worksheet.sheet_name = "#{task.name}"

        #создаем начальный заголовок
        worksheet.insert_cell(1, 0,  "CML ID идентификатор товара")
        worksheet.insert_cell(1, 1,  "Артикул")
        worksheet.insert_cell(1, 2,  "Название раздела")
        worksheet.insert_cell(1, 3,  "Дата добавления")
        worksheet.insert_cell(1, 4,  "Дата обновления")
        worksheet.insert_cell(1, 5,  "Название производителя")
        worksheet.insert_cell(1, 6,  "Файл изображения для товара")
        worksheet.insert_cell(1, 7,  "Дополнительные фото")
        worksheet.insert_cell(1, 8,  "Год издания")
        worksheet.insert_cell(1, 9,  "url")
        worksheet.insert_cell(1, 10, "Цена лидеркинг")
        worksheet.insert_cell(1, 11, "Идентификатор валюты")
        worksheet.insert_cell(1, 12, "Склад Основной")
        worksheet.insert_cell(1, 14, "Активность товара")
        worksheet.insert_cell(1, 13, "CML GROUP ID идентификатор группы товаров")
        worksheet.insert_cell(1, 15, "Файл малого изображения для товара")

        last_category_id = nil
        col              = 1
        last_row         = 15
        row_hash         = {}
        notify_p         = {:upd => [], :new => [], :old => [], :del=> []}

        task.products.order(:category_id).each do |product|

          col+=1
          if last_category_id != product.category_id
            last_category_id = product.category_id
            worksheet.insert_cell(col, 2, product.category.name)
            worksheet.insert_cell(col, 13, product.category.uid)
            col+=1
          end

          worksheet.insert_cell(col, 0, "sk#{product.id}")
          worksheet.insert_cell(col, 1, product.sku)
          worksheet.insert_cell(col, 3, product.created_at.strftime("%d/%m/%Y") )
          worksheet.insert_cell(col, 4, product.updated_at.strftime("%d/%m/%Y") )
          worksheet.insert_cell(col, 5, "Старая книга")
          worksheet.insert_cell(col, 9, product.url)
          worksheet.insert_cell(col, 11, "1")
          worksheet.insert_cell(col, 12, "1")
          worksheet.insert_cell(col, 13, product.category.uid)

          if product.status == 'del'
            worksheet.insert_cell(col, 14, 0)
          else
            worksheet.insert_cell(col, 14, product.sale)
          end

          if !product.j['price']['value'].blank?
            if product.j['price']['value'] < 1000000
              worksheet.insert_cell(col, 10, product.j['price']['value'] * 1.4)
            else
              worksheet.insert_cell(col, 10, product.j['price']['value'] * 1.2)
            end
          end

          if product.imgs.count >= 1
            worksheet.insert_cell(col, 6,  product.imgs[0])
            worksheet.insert_cell(col, 15, product.imgs[0])
          end

          if product.imgs.count >= 2
            worksheet.insert_cell(col, 7, product.imgs[1])
          end

          if product.year.count >= 1
            worksheet.insert_cell(col, 8, product.year[0])
          end

          product.j.each do |pj|
            if row_hash[pj[0]].nil?
              last_row += 1
              row_hash[pj[0]] = last_row
              worksheet.insert_cell(1, row_hash[pj[0]], pj[1]['name'])
            end

            if product.status == 'new'
              worksheet.insert_cell(col, row_hash[pj[0]], pj[1]['value'])
              worksheet.sheet_data[col][row_hash[pj[0]]].change_fill('2ecc71')

            elsif product.status == "upd" && product.upd_fields.include?(pj[0])
              worksheet.insert_cell(col, row_hash[pj[0]], pj[1]['value'])
              worksheet.sheet_data[col][row_hash[pj[0]]].change_fill('e67e22')
            else
              worksheet.insert_cell(col, row_hash[pj[0]], pj[1]['value'])
            end

          end

          if product.imgs.count > 2
            product.imgs.each_with_index.map{|v,i|
              if i > 1
                col+=1
                worksheet.insert_cell(col, 0, "sk#{product.id}")
                worksheet.insert_cell(col, 7, v)
              end
            }
          end

          if product.year.count > 1
            product.year.each_with_index.map{|v,i|
              if i > 0
                col+=1
                worksheet.insert_cell(col, 0, "sk#{product.id}")
                worksheet.insert_cell(col, 8, v)
              end
            }
          end

          if product.status == 'new'
            notify_p[:new] << product
          elsif product.status == 'upd'
            notify_p[:upd] << product
          elsif product.status == 'del'
            notify_p[:del] << product
          elsif product.status == 'old'
            notify_p[:old] << product
          end

          product.status     = 'del'
          product.upd_fields = []
          product.save

        end#end task

        workbook.write("#{Rails.root}/public/oldbooks/xlsx/#{file_name}")
      end

      files[current_file.task_id] = notify_p
    end

    email = Setting.find_by(:name => 'email')
    puts "Отправляем почту на - #{email}".colorize(:green)
    Notification.mail_files(email.v, files_task, files, "oldbooks")

  end
end
