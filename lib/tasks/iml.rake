def gs(z, h={})
  begin
    Geocoder.search(z, h)
  rescue Exception => e
    sleep 5
    ap e
    gs(z, h)
  end
end

namespace :iml do

  desc "обновление iml.ru"
  task :upd => :environment do
    Iml::Point.delete_all
    Iml::City.delete_all

    require "open-uri"
    require 'sanitize'

    str = open('https://iml.ru/files/pvz.js').read
    str.gsub!("(function() {\n    var pvz = ", "")
    str.gsub!(";\n    var courierCities = [];\n\n    if(typeof window.IML !== \"object\") {\n        window.IML = {}\n    }\n    window.IML.pvz = pvz;\n    window.IML.courierCities = courierCities;\n})();", '')

    json = JSON.parse(str)
    json = Hash[json.map{|i| [i[0], i[1]]}]
    str.clear
    ###################

    if 1 == 1
      json.each do |city|
        #name: string, prefix: string, zoom: string, lat: float, lon: float
        db_city   = Iml::City.find_or_create_by(:name => city[1]['name']['ru'])
        city_geo  = gs(db_city.name).first.data

        db_city.prefix = city[1]['prefix']
        db_city.zoom   = city[1]['zoom']
        db_city.lat    = city[1]['center'][1]# -><-
        db_city.lon    = city[1]['center'][0]# -><-
        db_city.geo    = city_geo
        db_city.save

        city[1]['markers'].each do |point|
          #name:  nil,  link: nil,  zoom: nil, address: nil, path: nil,     time: nil
          #photo: nil, metro: nil,   lat: nil,     lon: nil,  geo: nil,  city_id: nil
          if point['shortaddress']['ru'].blank?
            address = Sanitize.fragment(point['address']['ru'])
          else
            address = Sanitize.fragment(point['shortaddress']['ru'])
          end

          db_point = Iml::Point.new(
            :name    => point['name']['ru'],
            :link    => point['link'],
            :zoom    => point['zoom'],
            :address => Sanitize.fragment(point['address']['ru']),
            :path    => point['pathfull']['ru'],
            :time    => Sanitize.fragment(point['time']['ru']),
            :photo   => point['photo'],
            :lat     => point['position'][0],
            :lon     => point['position'][1],
            :city_id => db_city.id
          )


          ap point
          gs_str    = "#{db_city.geo['GeoObject']['name']}, #{db_point.address}"
          ap gs_str
          point_geo = gs(gs_str).first

          if gs(gs_str).first.nil?
            gs_str    = "#{db_city.geo['GeoObject']['name']}"
            ap nil
            ap gs_str
            point_geo = gs(gs_str).first
          end

          point_geo = point_geo.data

          db_point.geo = point_geo
          db_point.address_formatted = point_geo['GeoObject']['metaDataProperty']['GeocoderMetaData']['Address']['formatted']

          #metro
          if db_city.geo['GeoObject']['name'] == 'Москва' || db_city.geo['GeoObject']['name'] == 'Санкт-Петербург'
            metro_geo  = gs( point['position'].join(" "), :params=>{:kind=>'metro'} ).first
            puts "METRO"
            ap metro_geo
            if !metro_geo.nil?
              db_point.metro  = metro_geo.data['GeoObject']['name']
            end
          end

          begin
            db_point.save
          rescue Exception
            ap db_point
            raise ErrSave
          end

        end
      end
    end

    upd = []
    [ "http://#{Rails.application.config.my_host}/iml/points?gen=msk",
      "http://#{Rails.application.config.my_host}/iml/points?gen=spb",
      "http://#{Rails.application.config.my_host}/iml/points?gen=oth"].each_with_index.map do |url,i|

      body = open(url).read
      sha  = Digest::SHA1.hexdigest body
      if File.exist?("tmp/#{i}")
        file = open("tmp/#{i}").read
      else
        file =""
      end

      if file != sha
        File.open("tmp/#{i}", "w"){ |f|
          f.write(sha)
        }

        upd << url
      end
    end

    email = Setting.find_by(:name => 'email')
    puts "Отправляем почту на - #{email.v}".colorize(:green)
    Notification.mail_iml_new(email, upd)
  end


end
