namespace :artelbook do

  desc "Парсинг сайта http://www.artelbook.ru"
  task :parse, [:type] => :environment do |t, args|
    if File.exist?('tmp/pids/artelbook.pid')

    else
      File.write('tmp/pids/artelbook.pid', Process.pid)
      begin
        args.with_defaults(:type => 'all')

        ########################################################################
        str = 'Старт парсинга сайта http://www.artelbook.ru'
        puts str.colorize(:green)  ##
        tg = TelegramLog.new(str)
        @g = { :time_start => Time.now}                                       ##
        uuid = UUID.new                                                       ##
        ########################################################################

        tasks = Artelbook::Task.where(:enable => true)
        o     = ArtelbookParse.new()
        files_task = []
        files      = {}

        tasks.each do |task|
          puts "Задача #{task.name}".colorize(:green)

          #

          if args[:type] == 'gen'
            puts 'Пропуск парсинга!'.colorize(:yellow)
          elsif args[:type] == 'all'

            task.products.each do |product|
              product.status     = 'del'
              product.upd_fields = []
              product.save
            end

            paths = task.path.split(',')
            ap paths

            urls = o.cats(paths)
            urls.each do |url|
              products = o.products(url)

              if products[:products].blank?
                puts "Продуктов не найденно на #{url} / #{products[:path]} => #{products[:name]}".colorize(:yellow)
              else
                current_category = Artelbook::Category.set(products[:category])

                products[:products].each do |product_url|
                  product = o.get(product_url)
                  if product[:status]

                    #filter
                    sku   = product[:result][:sku].include?(task.sku_regex)
                    title = task.title_exclude.split(', ').map{|i| product[:result][:j]['title']["value"].include?(i) ? 1 : nil }.compact
                    year  = product[:result][:year].map{|i| i.to_i > 1940 ? 1 : nil}.compact

                    if sku && title.empty? && year.empty?
                      current_product = Artelbook::Product.set(product[:result], current_category.id, task.id)
                    else
                      filter = ""
                      if !sku
                        filter += "sku "
                      end
                      if !title.empty?
                        filter += "title "
                      end
                      if !year.empty?
                        filter += "year "
                      end
                      puts "Сработал фильтр #{filter}".colorize(:yellow)
                    end

                  else
                    puts "ошибка get - #{product_url}".colorize(:red)
                  end
                end
              end
            end
          end


          #генерация файла
          file_name = "#{uuid.generate}.xlsx"
          files_task << {:path => file_name, :name => task.name}
          current_file = Artelbook::File.new(
            path:    file_name,
            task_id: task.id
          )

          if current_file.save
            workbook  = RubyXL::Workbook.new
            worksheet = workbook[0]
            worksheet.sheet_name = "#{task.name}"

            #создаем начальный заголовок
            worksheet.insert_cell(1, 0,  "CML ID идентификатор товара")
            worksheet.insert_cell(1, 1,  "Артикул")
            worksheet.insert_cell(1, 2,  "Название раздела")
            worksheet.insert_cell(1, 3,  "Дата добавления")
            worksheet.insert_cell(1, 4,  "Дата обновления")
            worksheet.insert_cell(1, 5,  "Название производителя")
            worksheet.insert_cell(1, 6,  "Файл изображения для товара")
            worksheet.insert_cell(1, 7,  "Дополнительные фото")
            worksheet.insert_cell(1, 8,  "Год издания")
            worksheet.insert_cell(1, 9,  "url")
            worksheet.insert_cell(1, 10, "Цена товара")
            worksheet.insert_cell(1, 11, "Идентификатор валюты")
            worksheet.insert_cell(1, 12, "Склад Основной")
            worksheet.insert_cell(1, 14, "Активность товара")
            worksheet.insert_cell(1, 13, "CML GROUP ID идентификатор группы товаров")
            worksheet.insert_cell(1, 15, "Файл малого изображения для товара")
            worksheet.insert_cell(1, 16, "Новинка/Обновление")

            last_category_id = nil
            col              = 1
            last_row         = 16
            row_hash         = {}
            notify_p         = {:upd => [], :new => [], :old => [], :del=> []}

            #clear old
            task.products.each do |product|

            end

            task.products.order(:category_id).each do |product|

              col+=1
              if last_category_id != product.category_id
                last_category_id = product.category_id
                worksheet.insert_cell(col, 2, product.category.name)
                worksheet.insert_cell(col, 13, product.category.uid)
                col+=1
              end

              worksheet.insert_cell(col, 0, "ar#{product.id}")
              worksheet.insert_cell(col, 1, product.sku)
              worksheet.insert_cell(col, 3, product.created_at.strftime("%d/%m/%Y") )
              worksheet.insert_cell(col, 4, product.updated_at.strftime("%d/%m/%Y") )
              worksheet.insert_cell(col, 5, "Артельбук")
              worksheet.insert_cell(col, 9, product.url)
              worksheet.insert_cell(col, 11, "1")
              worksheet.insert_cell(col, 12, "1")
              worksheet.insert_cell(col, 13, product.category.uid)


              if product.status == 'del'
                worksheet.insert_cell(col, 14, 0)
              else
                worksheet.insert_cell(col, 14, product.sale)
              end

              case product.status
              when 'upd'
                worksheet.insert_cell(col, 16, "Изменено")
              when 'new'
                worksheet.insert_cell(col, 16, "Добавлено")
              when 'old'
                worksheet.insert_cell(col, 16, "Без изменений")
              when 'del'
                worksheet.insert_cell(col, 16, "Удалён")
              end

              if !product.j['price']['value'].blank?
                if task.price_ratio.nil?
                  worksheet.insert_cell(col, 10, product.j['price']['value'])
                else
                  worksheet.insert_cell(col, 10, product.j['price']['value'] * task.price_ratio)
                end
              end

              if product.imgs.count >= 1
                worksheet.insert_cell(col, 6,  product.imgs[0])
                worksheet.insert_cell(col, 15, product.imgs[0])
              end

              if product.imgs.count >= 2
                worksheet.insert_cell(col, 7, product.imgs[1])
              end

              if product.year.count >= 1
                worksheet.insert_cell(col, 8, product.year[0])
              end

              product.j.each do |pj|
                if row_hash[pj[0]].nil?
                  last_row += 1
                  row_hash[pj[0]] = last_row
                  worksheet.insert_cell(1, row_hash[pj[0]], pj[1]['name'])
                end

                if product.status == 'new'
                  worksheet.insert_cell(col, row_hash[pj[0]], pj[1]['value'])
                  worksheet.sheet_data[col][row_hash[pj[0]]].change_fill('2ecc71')

                elsif product.status == "upd" && product.upd_fields.include?(pj[0])
                  worksheet.insert_cell(col, row_hash[pj[0]], pj[1]['value'])
                  worksheet.sheet_data[col][row_hash[pj[0]]].change_fill('e67e22')
                else
                  worksheet.insert_cell(col, row_hash[pj[0]], pj[1]['value'])
                end

              end

              if product.imgs.count > 2
                product.imgs.each_with_index.map{|v,i|
                  if i > 1
                    col+=1
                    worksheet.insert_cell(col, 0, "ar#{product.id}")
                    worksheet.insert_cell(col, 1, product.sku)
                    worksheet.insert_cell(col, 7, v)
                  end
                }
              end

              if product.year.count > 1
                product.year.each_with_index.map{|v,i|
                  if i > 0
                    col+=1
                    worksheet.insert_cell(col, 0, "ar#{product.id}")
                    worksheet.insert_cell(col, 1, product.sku)
                    worksheet.insert_cell(col, 8, v)
                  end
                }
              end


              if product.status == 'new'
                notify_p[:new] << product
              elsif product.status == 'upd'
                if product.upd_fields == ["url", "path"]
                  notify_p[:old] << product
                else
                  notify_p[:upd] << product
                end
              elsif product.status == 'del'
                notify_p[:del] << product
              elsif product.status == 'old'
                notify_p[:old] << product
              end

              if  product.status == 'del'
                product.delete
              else
                product.save
              end

            end#end task

            workbook.write("#{Rails.root}/public/artelbook/xlsx/#{file_name}")
          end

          files[current_file.task_id] = notify_p
        end

        email = Setting.find_by(:name => 'email')
        puts "Отправляем почту на - #{email.v}".colorize(:green)
        Notification.mail_files(email.v, files_task, files, "artelbook")

      rescue Exception => e
        tg.send("Exception \n #{e.message}")
        tg.send(e.backtrace.join("\n"))
        raise e
      end


      File.unlink('tmp/pids/artelbook.pid')
    end
  end



  task :urls, [:type] => :environment do |t, args|
    args.with_defaults(:type => 'all')

    ########################################################################
    str = 'Старт парсинга сайта http://www.artelbook.ru'
    puts str.colorize(:green)  ##
    @g = { :time_start => Time.now}                                       ##
    uuid = UUID.new                                                       ##
    ########################################################################

    tasks = Artelbook::Task.where(:enable => true)
    o     = ArtelbookParse.new()
    files_task = []
    files      = {}

    tasks.each do |task|
      puts "Задача #{task.name}".colorize(:green)
      #
      #

      paths = task.path.split(',')
      ap paths

      urls = o.cats(paths)
      urls.each do |url|
        products = o.products(url)

        if products[:products].blank?
          puts "Продуктов не найденно на #{url} / #{products[:path]} => #{products[:name]}".colorize(:yellow)
        else
          current_category = Artelbook::Category.set(products[:category])

          products[:products].each do |product_url|
            ap product_url



          end
        end
      end
    end
  end

end
