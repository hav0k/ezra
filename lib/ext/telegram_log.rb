require 'telegram/bot'

class TelegramLog
  def initialize(msg=nil)
    @tg_token    = Setting.get('telegram_token')
    @tg_chat_id  = Setting.get('telegram_chat_id')
    @send_ids    = []
    @last_msg    = ""

    if !msg.nil?
      send(msg)
    end
  end

  def sended_ids
    @send_ids
  end

  def send(msg)
    result = Telegram::Bot::Client.run(@tg_token) do |bot|
      bot.api.send_message(chat_id: @tg_chat_id, text: m(msg))
    end

    if result['ok']
      @send_ids << result['result']['message_id']
    end
  end

  def edit_last(msg)
    if @last_msg != m(msg)
      edit(@send_ids.last, msg)
    end
  end

  def edit(id, msg)
    Telegram::Bot::Client.run(@tg_token) do |bot|
      bot.api.editMessageText(chat_id: @tg_chat_id, text: m(msg), message_id: id)
    end
  end

  def progress(percent=nil)
    str = (0..9).map{"░"}.join
    if percent.nil?
      send("#{str} 0%")
    else
      0.upto(percent/10-1).map{|i| str[i] = "█"}
      edit_last("#{str} #{percent}%")
    end
  end

  private

    def m(msg)
      @last_msg = msg[0,4000]
      @last_msg
    end


end
