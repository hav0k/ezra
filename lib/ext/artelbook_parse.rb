class ArtelbookParse

  def initialize(url=false)
    @m = Mechanize.new { |agent|
      agent.user_agent_alias = 'Mac Safari'
    }
    set_url(url)
  end

  def cats(paths)
    urls = paths.map do |path|
      set_url("http://artelbook.ru/#{path.strip}") #knigi-po-kategoriyam
      @page.xpath('//div[contains(@class, "category")]/div/div/a').map{|i| i.attr('href')}
    end
    ap urls

    urls.flatten
  end

  def products(url)
    r={:products => [], :category => {:name => nil, :path => nil}}
    set_url(url)

    #paginate

    #curren category
    r[:category][:path] = @page.uri.path.split('/')[-1]
    r[:category][:name] = @page.xpath('//div[@class="browse-view"]/h1').text

    #products
    r[:products] += @page.xpath('//div[@class="info-product"]/div/a').map{|i| i.attr('href')}


    #.map{|i|
    #  "#{uri.scheme}://#{uri.host}#{i.attr('href')}"
    #}

    r
  end


  def get(url)
    set_url(url)
    uri = @page.uri
    ap url

    if @page.title == '404' || @page.uri.path != url
      puts "404!".colorize(:red)
      r false, {:code => '404'}
    else
      if @page.xpath('//div[@class="stock-status"]/span').attr('class').text == 'in-stock'
        sale  = 1
        price = @page.xpath('//span[@class="PricesalesPrice" and @itemprop="price"]').text.to_i
      elsif @page.xpath('//div[@class="stock-status"]/span').attr('class').text == 'out-stock'
        sale  = 0
        price = nil
      else
        ap @page.xpath('//div[@class="stock-status"]/span')
        raise StockErr
      end

      pp = @page.xpath('//div[contains(@class, "product-description")]/p').map{|i| i.text.strip}.join("\n ")

      h = {
        :id     => @page.xpath('//input[@name="virtuemart_product_id[]"]').attr('value').text,
        :sku    => @page.xpath('//div[@class="spacer-buy-area"]/div[@class="pseudo_h3"]').text.gsub('Артикул: ', ''),
        :url    => uri.to_s,
        :path   => uri.path,
        :imgs   => @page.xpath('//div[@class="wrapper-zoom-carousel"]/div/div[@class="floatleft item"]/a').map{|i| 'http://artelbook.ru' + i.attr('href')},
        :sale   => sale,
        :j => {
          "title"  => {
            "name"  => "Название товара",
            "value" => @page.xpath('//div[@class="spacer-buy-area"]/h1').text
          },
          "price"  => {
            "name"  => "Цена товара",
            "value" => price,
          },
          "p2"   => {
            "name"  => "Текст",
            "value" => pp
          }

        }
      }

      h[:j].merge!(
        @page.xpath('//div[contains(@class, "product-description")]/ul/li').map{|i|
          li = i.text.split(':').map{|i| i.strip}

          if li.count == 1
            #adv << li[1]
          elsif li.count > 1
            [
              Translit.convert(li[0]).gsub(/["'«»,]/,'').gsub(/[ -]/,'_').downcase,
              {
                "name"  => li[0],
                "value" => li[1, li.count-1].join(': ')
              }
            ]
          end
        }.compact.to_h
      )

      if !h[:j]["god_izdaniya"].nil?
        ya = h[:j]["god_izdaniya"]['value'].scan(/\d{4}/)
        h[:year] = ya
        h[:j].delete("god_izdaniya")
      else
        h[:year] = []
      end

      r true, h
    end


  end


  private

    def set_url(u, s=0)
      begin
        sleep s
        if u
          @page = @m.get u
        end
        sleep Setting.find_by(:name => 'sleep_get').v.to_f / 1000
      rescue Exception => e
        r_agent = Mechanize::AGENT_ALIASES.map{|i| i[0]}.shuffle[0]
        @m.cookie_jar.clear!
        puts e.to_s.colorize(:red)
        s += 1
        sleep s
        set_url(u, s)
      end


    end

    def r status, message
      {:status => status, :result => message}
    end

end
