class EbayParse

  def initialize(proxy=false)
    @proxy = URI.parse("http://10.10.20.1:8888")
  end

  def get(q=nil)
    q_strong = q.split.map{|i| "\"#{i}\" "}.join.strip
    link   ="https://www.ebay.com/sch/Antiquarian-Collectible/29223/i.html?_from=R40&_nkw=#{q_strong}"
    e_link = URI.encode(link)
    p e_link
    doc = Nokogiri::HTML::Document.parse( open( e_link ), nil, "UTF-8")
    #a = 0

    if doc.text.index("0 результатов найдено в категории").nil?

      results = doc.xpath("//ul[@id='ListViewInner']/li[contains(@class, 'sresult')]").map { |i|

        #if i.attr('r').nil?
        #  a = i.attr('r')
        #end

        #if a == 0
          {
            :q     => q,
            :link  => e_link,
            :id    => i.attr('listingid'),
            :title => i.xpath(".//h3/a").text,
            :img   => i.xpath(".//img[@class='img']").attr('src').text,
            :price => i.xpath(".//li[@class='lvprice prc']/span").text
          }
        #end
      }.compact

    else
      results = []
    end

    p results

    r true, results
  end

  private

    def r status, message
      {:status => status, :result => message}
    end

end
