class NewauctionParse

  def initialize(proxy=false)
    @proxy = URI.parse("http://10.10.20.1:8888")
  end


  #До 1700 г.
  #http://newauction.ru/listing/offer/do_1700_g-105062?flt_prp_offerstate=0&flt_prp_sesend=gt__macro_now&flt_prp_sesstart=lt__macro_now&flt_sin_11019400742342544=match_%D0%B4%D1%80%D0%B5%D0%B2%D0%BD%D1%8F%D1%8F&srt_prp_sesend=asc&ipp=180
  #1701-1800 гг.
  #http://newauction.ru/listing/offer/1701_1800_gg-105063?flt_prp_offerstate=0&flt_prp_sesend=gt__macro_now&flt_prp_sesstart=lt__macro_now&flt_sin_11019400742342544=match_%D0%B4%D1%80%D0%B5%D0%B2%D0%BD%D1%8F%D1%8F&srt_prp_sesend=asc&ipp=180
  #1801-1854 гг.
  #http://newauction.ru/listing/offer/1801_1854_gg-100335?flt_prp_offerstate=0&flt_prp_sesend=gt__macro_now&flt_prp_sesstart=lt__macro_now&flt_sin_11019400742342544=match_%D0%B4%D1%80%D0%B5%D0%B2%D0%BD%D1%8F%D1%8F&srt_prp_sesend=asc&ipp=180
  #1855-1900 гг.
  #http://newauction.ru/listing/offer/1855_1900_gg-100336?flt_prp_offerstate=0&flt_prp_sesend=gt__macro_now&flt_prp_sesstart=lt__macro_now&flt_sin_11019400742342544=match_%D0%B4%D1%80%D0%B5%D0%B2%D0%BD%D1%8F%D1%8F&srt_prp_sesend=asc&ipp=180
  #1901-1917 гг.
  #http://newauction.ru/listing/offer/1901_1917_gg-100338?flt_prp_offerstate=0&flt_prp_sesend=gt__macro_now&flt_prp_sesstart=lt__macro_now&flt_sin_11019400742342544=match_%D0%B4%D1%80%D0%B5%D0%B2%D0%BD%D1%8F%D1%8F&srt_prp_sesend=asc&ipp=180
  #1918-1940 гг.
  #http://newauction.ru/listing/offer/1918_1940_gg-100339?flt_prp_offerstate=0&flt_prp_sesend=gt__macro_now&flt_prp_sesstart=lt__macro_now&flt_sin_11019400742342544=match_%D0%B4%D1%80%D0%B5%D0%B2%D0%BD%D1%8F%D1%8F&srt_prp_sesend=asc&ipp=180

  def get(q=nil)
    m = Mechanize.new { |agent|
      agent.user_agent_alias = 'Mac Safari'
    }

    link=["http://newauction.ru/listing/offer/", "?flt_prp_offerstate=0&flt_prp_sesend=gt__macro_now&flt_prp_sesstart=lt__macro_now&flt_sin_11019400742342544=match_#{q}&srt_prp_sesend=asc&ipp=180"]
    cats = ['do_1700_g-105062', '1701_1800_gg-105063', '1801_1854_gg-100335', '1855_1900_gg-100336', '1901_1917_gg-100338', '1918_1940_gg-100339']
    
    results = []
    cats.each do |cat|
      e_link = URI.encode("#{link[0]}#{cat}#{link[1]}")
      p e_link

      #encode! 
      #doc = Nokogiri::HTML::Document.parse( open( e_link ), nil, "UTF-8")
      #doc = Nokogiri::HTML(open(e_link), nil, Encoding::UTF_8.to_s)
      begin
        doc = m.get( e_link )
        doc.xpath("//div[@class='listing']/div/div[contains(@class, 'public_offer_snippet_container')]").each do |item|
          results << {
            :q      => q,
            :link   => e_link,
            :id     => item.attr('data-id'),
            :title  => item.xpath(".//div[contains(@class, 'h3')]/a").text,
            :img    => item.xpath(".//div[contains(@class, 'snippet_photo')]").attr('data-original').text,
            :price  => item.xpath(".//div[@class='price']/span").text.gsub(" ", '').to_i
          }
        end
        
      rescue Exception => e
        
      end

    end

    p results
    puts "______________\n______________"

    r true, results

  end

  private

    def r status, message
      {:status => status, :result => message}
    end

end
