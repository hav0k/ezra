class BidspiritParse
  def initialize(proxy=false)
    @m = Mechanize.new { |agent|
      agent.user_agent_alias = 'Mac Safari'
    }
    @m.agent.set_socks('localhost', 9050)
  end


  def get(q)
    q.gsub!(" ", "+")
    link = [
      "https://ru.bidspirit.com/services/catalogs/searchItemsWithSearchServer/?",
      "contentType=ART",
      "&house=all",
      "&lang=ru",
      "&limit=50",
      "&region=RU",
      "&skip=0",
      "&templateCode=by_relevance",
      "&time=FUTURE",
      "&token=#{q}"
    ].join("")

    p link
    doc=tor_get(link)

    r = JSON.parse(doc.body)
    if r['count'] > 0
      r = JSON.parse r['hits']
      r true, r.map {|i|

          if i['_source']['houseCode'].nil?
            img = "https://res.cloudinary.com/bidspirit/image/upload/q_80,w_400,h_400,c_fit/moscowbooks_auction_#{i['_source']['auctionIdInApp']}_lots_import_0_#{i['_source']['itemIndex']}_1.jpg"
          else
            img_name = i['_source']['imagesList'].first
            if img_name.nil?
              img = nil
            else
              id  = img_name.split('.')[0]
              img = "https://bidspirit-images.global.ssl.fastly.net/#{i['_source']['houseCode']}/cloned-images/#{i['_source']['idInApp']}/#{id}/a_ignore_q_80_w_400_h_400_c_fit_#{id}.jpg"
            end
          end

          {
            :q     => q,
            :link  => link,
            :id    => i['_id'],
            :href  => "https://ru.bidspirit.com/portal/#!/lotPage/source/catalog/auction/#{i['_source']['ownerKey']}/lot/#{i['_source']['idInApp']}/",
            :title => i['_source']['name']['ru'].blank? ? i['_source']['description']['ru'][0,128] : i['_source']['name']['ru'],
            :img   => img,
            :price => i['_source']['estimatedPrice']
          }
      }.compact
    else
      r true, []
    end

  end


    def switch_endpoint
      localhost = Net::Telnet::new("Host" => "localhost", "Port" => "9051", "Timeout" => 10, "Prompt" => /250 OK\n/)
      localhost.cmd('AUTHENTICATE ""') { |c| print c; throw "Cannot authenticate to Tor" if c != "250 OK\n" }
      localhost.cmd('signal NEWNYM') { |c| print c; throw "Cannot switch Tor to new route" if c != "250 OK\n" }
      localhost.close
    end

    def tor_get(l)
      begin
        doc=@m.get(l)

        doc
      rescue Exception => e
        puts "Ошибка #{e.message}".colorize(:red)
        switch_endpoint
        sleep 5
        tor_get(l)
      end

    end

  private

    def r status, message
      {:status => status, :result => message}
    end
end
