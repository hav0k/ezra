class OldbookParse

  def initialize(url=false)
    @m = Mechanize.new { |agent|
      agent.user_agent_alias = 'Mac Safari'
    }
    set_url(url)
  end

  def cats(url)
    set_url(url)
    @page.xpath('//ul[@class="bx_sitemap_ul"]/a[@class="catalogItem"]').map{|p| p.attr('href')}
  end

  def products(url)
    r={:products => [], :category => {:name => nil, :path => nil}}
    set_url(url)
    #paginate
    end_page  = @page.xpath('//div[@class="bx-pagination-container row"]/ul/li[@class=""][last()]').text.to_i
    if end_page == 0
      end_page = 1
    end

    #curren category
    uri      = @page.uri
    r[:category][:path] = uri.path.split('/')[-1]
    r[:category][:name] = @page.xpath('//h2[@class="subheading"]').text.gsub('Старая книга - раздел', '').strip

    #products
    1.upto(end_page) do |page_n|
      if page_n != 1
        uri       =  @page.uri
        uri.query = "PAGEN_1=#{page_n}"
        set_url(uri)
        p @page.uri
      end

      r[:products] += @page.xpath('//div[@class="catalog"]/a[@class="catalogItem"]').map{|i|
        "#{uri.scheme}://#{uri.host}#{i.attr('href')}" 
      }

    end
    r
  end


  def check(url=false)
    set_url(url)

    if !@page.xpath('//div[@class="catalog"]/a[@class="catalogItem"]').blank?
      "products"
    elsif !@page.xpath('//ul[@class="bx_sitemap_ul"]/a[@class="catalogItem"]').blank?
      "catalogs"
    else
      false
    end

  end


  def get(url)
    set_url(url)
    uri = @page.uri

    p url

    if @page.xpath('//div[@class="productBtnWrap"]/a[1]').blank? 
      id = @page.body.scan(/var productId = (\d{1,});/i)[0][0].to_i
      sale = 0
    else
      sale = 1
      id   = @page.xpath('//div[@class="productBtnWrap"]/a[1]').attr('onclick').text.gsub('javascript:addBasket(', '').to_i
    end

    p1 = @page.xpath('//span[@class="productPrice"]/following::p[1]').inner_html.strip
    p2 = @page.xpath('//span[@class="productCharactInfo"]/following::p[1]').inner_html.strip
    if p1 != p2 && !p2.blank?
      pp = p1 + "\r\n" + p2
    else
      pp = ""
    end

    h = {
      :id     => id,
      :sku    => nil,
      :url    => uri.to_s,
      :path   => uri.path.split('/')[-1],
      :imgs   => @page.xpath('//div[@class="big-img"]/a').map { |i| "http://oldbooks.ru" + i.attr('href') }.compact,
      :sale   => sale,
      :j => {
        #  'sale'   => {
        #  "name"  => "В наличии",
        #  "value" => sale
        #},
        "title"  => {
          "name"  => "Название товара",
          "value" => @page.xpath('//span[@class="productHeading"]').text.strip
        },
        "price"  => {
          "name"  => "Цена товара",
          "value" => @page.xpath('//span[@class="productPrice"]'  ).text.gsub(' ', '').to_i,
        },
        "p1"   => {
          "name"  => "Описание товара",
          "value" => p1
        },
        "p2"   => {
          "name"  => "Текст",
          "value" => pp
        }
      }
    }

    h[:j].merge!(
      @page.xpath('//span[@class="productCharactInfo"]/span[@class="name_harakreristiki"]').map{|i|
                  
        if i.text == "Переплет"
          i_text = "Описание Переплета"
        elsif i.text == "Кол-во страниц"
          i_text = "Количество страниц"
        elsif i.text == "Формат/Размер (см)"
          i_text = "Формат"
        else 
          i_text = i.text
        end

        [
          Translit.convert(i.text).gsub(" ","_").gsub("-","_").gsub("\'",'').gsub("\"",'').downcase,          
          { 
            "name"  => i_text,
            "value" => i.xpath('./following::span[1]').text
          }
        ]
      }.to_h
    )

    if !h[:j]['artikul'].nil? && h[:j]['artikul']['value'].include?('if-')
      h[:sale] = 0
    end
    
    if !h[:j]['artikul'].nil?
      h[:sku] = h[:j]['artikul']['value']
      h[:j].delete('artikul')
    end



    if !h[:j]['mesto_izdaniya'].nil? && !h[:j]['mesto_izdaniya']['value'].nil?
      Setting.find_by(name: "change_option_val").vh.map{ |hh| 
        if h[:j]['mesto_izdaniya']['value'] == hh['find']
          h[:j]['mesto_izdaniya']['value'] = hh['value']
        end
      }
    end


    if !h[:j]["god_izdaniya"].nil?
      ya = h[:j]["god_izdaniya"]['value'].scan(/\d{4}/)
      h[:year] = ya
      h[:j].delete("god_izdaniya")
    else
      h[:year] = []
    end

    r true, h
  end


  private

    def set_url(u, s=0)

      begin
        sleep s
        if u
          @page = @m.get u
        end
        sleep Setting.find_by(:name => 'sleep_get').v.to_f / 1000
      rescue Exception => e
        r_agent = Mechanize::AGENT_ALIASES.map{|i| i[0]}.shuffle[0]
        @m.cookie_jar.clear!
        puts e.to_s.colorize(:red)
        s += 1
        sleep s
        set_url(u, s)
      end
    end

    def r status, message
      {:status => status, :result => message}
    end

end
