class AlibParse

  def initialize(proxy=false)
    @m = Mechanize.new { |agent|
      agent.user_agent_alias = 'Mac Safari'
    }
    @m.agent.set_socks('localhost', 9050)
  end

  def get(q=nil, shop=false)
    @q=q #!!!
    results = []

    if shop
      link = "http://www.alib.ru/bs.php4?bs=#{shop}"
      @e_link = URI.encode(link)
      p @e_link
      doc = tor_get(@e_link)
      s_links = doc.xpath('//p/b[contains(text(), "По цене")]/parent::p/following::ul/li/a').map{|a| a.attr('href')}

      links = []
      s_links.map{|s_link|
        p s_link
        doc = tor_get(s_link)
        i_links = doc.xpath('//b[contains(text(),"Cтраницы:  1 |")]/a').map{|i| i.attr('href')}
        if i_links.count > 1
          uri  = URI.parse( URI.encode i_links[-1] )
          href = CGI::parse(uri.query)
          href['n9'][0] = href['all'].first.to_i - 60
          uri.query = URI.encode_www_form(href)
          i_links[-1] = uri.to_s
        end

        links << s_link
        links = links + i_links

      }

      ap links
    else
      # link = "http://www.alib.ru/findp.php4?title=#{@q.encode('windows-1251', { :invalid => :replace, :undef => :replace, :replace => '?' })}&tipfind=t46"
      link = "http://www.alib.ru/find3.php4?tfind=#{@q.encode('windows-1251', { :invalid => :replace, :undef => :replace, :replace => '?' })}"
      @e_link = URI.encode(link)
      p @e_link
      doc   = tor_get(@e_link)
      links = doc.xpath('//b[contains(text(),"Cтраницы:  1 |")]/a[position() < last()]').map{|i| i.attr('href')}
    end

    ##if !links.blank?
    ##  uri  = URI.parse( URI.encode links.first )
    ##  href = CGI::parse(uri.query)
    ##  if !shop
    ##    href.delete('bs')
    ##  end
    ##  count_page = (href['all'].first.to_i / 60.0).ceil
    ##
    ##  nlinks = []
    ##  1.upto(count_page-1) { |cp|
    ##    href['n9'][0] = (cp * 60).to_s
    ##    uri.query = URI.encode_www_form(href)
    ##    nlinks << uri.to_s
    ##  }
    ##
    ##  results += res(doc)
    ##  puts "#{results.count}"
    ##end

    if !shop
      results = res(doc)
      puts "#{results.count}"
    end

    if shop && !links.blank?
      links.each do |l|
        @e_link = URI.encode(l)
        p @e_link
        doc    = tor_get(@e_link)
        result = res(doc)
        if result.count == 0
          doc    = tor_get(@e_link, true)
          result = res(doc)
          if result.count == 0
            doc    = tor_get(@e_link, true)
            result = res(doc)
          end
        end
        results += result

        puts "#{result.count} => #{results.count}"
        sleep Setting.get("sleep_get_paginate") / 1000.0
      end
    end

    r true, results
  end

  def switch_endpoint
    localhost = Net::Telnet::new("Host" => "localhost", "Port" => "9051", "Timeout" => 10, "Prompt" => /250 OK\n/)
    localhost.cmd('AUTHENTICATE ""') { |c| print c; throw "Cannot authenticate to Tor" if c != "250 OK\n" }
    localhost.cmd('signal NEWNYM') { |c| print c; throw "Cannot switch Tor to new route" if c != "250 OK\n" }
    localhost.close
    sleep 5
  end

  private

    def r status, message
      {:status => status, :result => message}
    end

    def res(doc)
      doc.xpath('//a').map {|i|
        if i.text == "Купить"
          {
            :q     => @q,
            :link  => @e_link,
            :id    => i.attr('href').split('_')[-1].gsub('.html', ''),
            :href  => i.attr('href'),
            :title => i.parent.xpath('./b[1]').text.strip,
            :img   => i.parent.xpath('./a').map {|t| t.text.mb_chars.downcase.to_s.include?('фото') ? t.attr('href'): nil}.compact[0],
            :price => i.parent.text.match(/Цена\:\ (\d{1,10})\ руб\./i)[1]
          }
        end
      }.compact
    end

    def tor_get(l, se=false)

      if se
        switch_endpoint
      end

      begin
        doc=@m.get(l)

        if doc.body.size < 5000
          puts "Размер страницы менее 5000".colorize(:red)
          ap doc.body
          switch_endpoint
          sleep 10
          tor_get(l, false)

        elsif doc.xpath("//p[1]").text == "Заблокировано за слишком частое обращение к сайту!"
          puts "Забаннены! Меняем ip".colorize(:red)
          switch_endpoint
          sleep 5
          tor_get(l, false)
        end
        sleep 1
        doc
      rescue Exception => e
        puts "Ошибка #{e.message}".colorize(:red)
        switch_endpoint
        sleep 5
        tor_get(l)
      end

    end

end
