class CreateGdrives < ActiveRecord::Migration
  def change
    create_table :gdrives do |t|
      t.string :name, default: nil, :null => false
      t.string :spreadsheet, default: nil, :null => false
      t.boolean :enable, default: false
      t.boolean :succes, default: false
      t.json :result

      t.timestamps
    end

    add_index :gdrives, :spreadsheet, :unique => true
  end
end
