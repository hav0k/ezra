class CreateNewauctionTasklogs < ActiveRecord::Migration
  def change
    create_table :newauction_tasklogs do |t|
      t.column  :i, :bigint
      t.integer :task_id
      t.integer :module_id

      t.timestamps
    end

    add_index :newauction_tasklogs, [:i, :task_id, :module_id], :unique => true 
    add_index :newauction_tasklogs, :task_id    
  end
end
