class AddFieldStat < ActiveRecord::Migration
  def change
    add_column :notifi_contents, :stats, :text, default: "{}"
  end
end
