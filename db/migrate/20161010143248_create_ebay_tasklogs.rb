class CreateEbayTasklogs < ActiveRecord::Migration
  def change
    create_table :ebay_tasklogs, :id => false  do |t|
      t.column  :i, :bigint
      t.integer :task_id
      t.integer :module_id

      t.timestamps
    end

    add_index :ebay_tasklogs, [:i, :task_id, :module_id], :unique => true 
    add_index :ebay_tasklogs, :task_id    
  end
end
