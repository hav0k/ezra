class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.string :name, default: nil, :null => false
      t.string :t, default: 'text'
      t.text :about
      t.text :v
      t.json :vh

      t.timestamps
    end

    add_index :settings, :name, :unique => true
  end
end
