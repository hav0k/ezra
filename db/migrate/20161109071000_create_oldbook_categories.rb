class CreateOldbookCategories < ActiveRecord::Migration
  def change
    create_table :oldbook_categories do |t|
      t.string :name, default:nil, null:false
      t.string :path, default:nil, null:false
      t.string :uid

      t.timestamps
    end

    add_index :oldbook_categories, :path, :unique => true

  end
end
