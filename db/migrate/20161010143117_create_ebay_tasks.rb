class CreateEbayTasks < ActiveRecord::Migration
  def change
    create_table :ebay_tasks do |t|
      t.string  :name
      t.string  :stat
      t.text    :q, default:nil

      ###################
      t.integer :count, :default => 0
      t.integer :interval
      t.boolean :active, :default => false
      t.integer :counter
      ##################
      t.json    :p
      t.json    :e
      ##################
      t.integer :user_id #<= μαγεία fo potato ;) 
      t.integer :notifi_template_id
      ##################
      t.datetime   :next_at, :default => "now()"


      t.timestamps
    end
    add_index :ebay_tasks, :user_id
  end
end
