class CreateAlibTasklogs < ActiveRecord::Migration
  def change
    create_table :alib_tasklogs do |t|
      t.string  :i
      t.integer :task_id
      t.integer :module_id

      t.timestamps
    end

    add_index :alib_tasklogs, [:i, :task_id, :module_id], :unique => true 
    add_index :alib_tasklogs, :task_id   
  end
end
