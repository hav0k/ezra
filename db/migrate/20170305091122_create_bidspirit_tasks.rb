class CreateBidspiritTasks < ActiveRecord::Migration
  def change
    create_table :bidspirit_tasks do |t|
      t.string  :name
      t.string  :stat
      t.text    :q, default:nil

      ###################
      t.integer :count, :default => 0
      t.integer :interval
      t.boolean :active, :default => false
      t.integer :counter
      t.integer :gdrive_id
      ##################
      t.json    :p
      t.json    :e
      ##################
      t.integer :notifi_template_id
      ##################
      t.datetime   :next_at, :default => "now()"

      t.timestamps
    end
  end
end


