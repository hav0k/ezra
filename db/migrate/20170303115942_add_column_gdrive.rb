class AddColumnGdrive < ActiveRecord::Migration
  def change
    add_column :alib_tasks, :gdrive_id, :integer
    add_column :avito_tasks, :gdrive_id, :integer
    add_column :ebay_tasks, :gdrive_id, :integer
    add_column :newauction_tasks, :gdrive_id, :integer
  end
end
