class CreateImlCities < ActiveRecord::Migration
  def change
    create_table :iml_cities do |t|
      t.string :name
      t.string :prefix
      t.string :zoom
      t.float :lat
      t.float :lon
      t.json :geo

      t.timestamps
    end
  end
end
