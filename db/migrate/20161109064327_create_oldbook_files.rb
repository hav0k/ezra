class CreateOldbookFiles < ActiveRecord::Migration
  def change
    create_table :oldbook_files do |t|
      t.string  :path
      t.integer :task_id, default:nil, null:false

      t.timestamps
    end

    add_index :oldbook_files, :path, :unique => true

  end
end
