class CreateNotifiContent < ActiveRecord::Migration
  def change
    create_table :notifi_contents do |t|
      t.text :content, default: "{}"
      t.integer :notifi_template_id, default: nil, null: false
      t.boolean :sends, default: false

      t.timestamps
    end

    add_index :notifi_contents, :notifi_template_id
    
  end
end
