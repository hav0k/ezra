class CreateOldbookProducts < ActiveRecord::Migration
  def change
    create_table :oldbook_products do |t|
      t.string  :url, default:nil, null:false      
      t.string  :path, default:nil, null:false
      t.integer :task_id, default:nil, null:false
      t.integer :category_id
      t.string  :uid
      t.boolean :parse, default:false
      t.json    :j
      t.string  :imgs, array: true, default: []
      t.string  :year, array: true, default: []      
      t.string  :upd_fields, array: true, default: []
      t.string  :status, default: 'new' #'upd', 'old' 

      t.timestamps
    end

    add_index :oldbook_products, :path, :unique => true

  end
end
