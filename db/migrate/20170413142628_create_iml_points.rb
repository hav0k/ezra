class CreateImlPoints < ActiveRecord::Migration
  def change
    create_table :iml_points do |t|
      t.string :name
      t.string :link
      t.string :zoom
      t.string :address
      t.string :address_formatted
      t.string :path
      t.string :time
      t.string :photo
      t.string :metro
      t.float :lat
      t.float :lon
      t.json :geo
      t.integer :city_id

      t.timestamps
    end
  end
end
