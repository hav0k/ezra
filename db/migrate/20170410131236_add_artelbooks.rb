class AddArtelbooks < ActiveRecord::Migration
  def change
    create_table :artelbook_tasks do |t|
      t.string :name
      t.string :path
      t.boolean :enable, default:false
      #################################
      t.float   :price_ratio, default: nil
      t.string  :sku_regex, default: nil
      t.text    :title_exclude, default: nil

      t.timestamps
    end
    add_index :artelbook_tasks, :path, :unique => true


    create_table :artelbook_files do |t|
      t.string  :path
      t.integer :task_id, default:nil, null:false

      t.timestamps
    end
    add_index :artelbook_files, :path, :unique => true


    create_table :artelbook_products do |t|
      t.string  :url, default:nil, null:false
      t.string  :path, default:nil, null:false
      t.integer :task_id, default:nil, null:false
      t.integer :category_id
      t.string  :uid
      t.boolean :parse, default:false
      t.json    :j
      t.string  :imgs, array: true, default: []
      t.string  :year, array: true, default: []
      t.string  :upd_fields, array: true, default: []
      t.string  :status, default: 'new' #'upd', 'old'
      t.integer :sale
      t.string  :sku

      t.timestamps
    end
    add_index :artelbook_products, :path, :unique => true


    create_table :artelbook_categories do |t|
      t.string :name, default:nil, null:false
      t.string :path, default:nil, null:false
      t.string :uid

      t.timestamps
    end
    add_index :artelbook_categories, :path, :unique => true

  end
end
