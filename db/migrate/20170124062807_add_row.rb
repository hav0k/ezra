class AddRow < ActiveRecord::Migration
  def change
    add_column :oldbook_products, :sale, :integer
    add_column :oldbook_products, :sku,  :string
  end
end
