class CreateNotifiTemplates < ActiveRecord::Migration
  def change
    create_table :notifi_templates do |t|
      t.string :name, default: nil, null: false
      t.json :s, default: {}, null: false
      t.integer :user_id, default: nil, null: false
      t.boolean :active, default: false
      t.text :log
      t.integer :count, default: 0, null: false
      t.integer :interval
      t.datetime :next_at, :default => "now()"

      t.timestamps
    end

    add_index :notifi_templates, :user_id
  end
end
