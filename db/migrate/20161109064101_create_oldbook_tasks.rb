class CreateOldbookTasks < ActiveRecord::Migration
  def change
    create_table :oldbook_tasks do |t|
      t.string :name
      t.string :path
      t.boolean :enable, default:false

      t.timestamps
    end

    add_index :oldbook_tasks, :path, :unique => true
  end
end
