require 'google/apis/sheets_v4'
require 'googleauth'
require 'googleauth/stores/file_token_store'

require 'fileutils'

OOB_URI = 'urn:ietf:wg:oauth:2.0:oob'
APPLICATION_NAME = 'Google Sheets API Erza'

# @return [Google::Auth::UserRefreshCredentials] OAuth2 credentials
def authorize
  client_id = Google::Auth::ClientId.new(
    "883681910587-b08miugkmd6ap4367i2jkgh5stj7qrfs.apps.googleusercontent.com", 
    "6bQrb0IIjRkuKzjZS_rNe1R-"
  )
  scope = [
    Google::Apis::SheetsV4::AUTH_SPREADSHEETS,
    Google::Apis::SheetsV4::AUTH_DRIVE,
  ]

  token_store = Google::Auth::Stores::FileTokenStore.new(file: 'config/sheets.googleapis.yaml')
  authorizer  = Google::Auth::UserAuthorizer.new(client_id, scope, token_store)
  user_id     = 'main'
  credentials = authorizer.get_credentials(user_id)

  if credentials.nil?
    url = authorizer.get_authorization_url(
      base_url: OOB_URI)
    puts "Open the following URL in the browser and enter the " +
         "resulting code after authorization"
    puts url
    code = gets
    credentials = authorizer.get_and_store_credentials_from_code(
      user_id: user_id, code: code, base_url: OOB_URI)
  end
  credentials
end

# Initialize the API
service = Google::Apis::SheetsV4::SheetsService.new
service.client_options.application_name = APPLICATION_NAME
service.authorization = authorize

# Prints the names and majors of students in a sample spreadsheet:
# https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
spreadsheet_id = '1Lly-Io3s02jCQGDNdDUPtOibK_y_ORfVXbXqYXtddFc'
response = service.get_spreadsheet_values(spreadsheet_id, "A1:A")
puts 'No data found.' if response.values.empty?
response.values.each do |row|
  puts "#{row[0]}"
end

